package lab3.controller;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.DirectoryChooser;
import lab3.model.Author;
import lab3.model.Painting;
import lab3.utils.AuthorFileFilter;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class DisplayPaintingController {
    private final static String CHARSET = "ISO-8859-2";

    private static String paintingsPath = null;
    private final List<Author> authors = new LinkedList<>();
    private Author currentAuthor;
    private int currentPaintingIndex = 0;

    @FXML
    private ChoiceBox<Author> selectAuthor;
    @FXML
    private Button nextPainting;
    @FXML
    private Button prevPainting;
    @FXML
    private ImageView painting;
    @FXML
    private Label paintingTitle;
    @FXML
    private Label paintingDescription;
    @FXML
    private Label paintingLocation;

    @FXML
    private void initialize() {
        paintingsPath = chooseDataLocation();
        verifyDataLocation();
        loadData();
    }

    @FXML
    private void selectAuthorHandler() {
        currentAuthor = selectAuthor.getSelectionModel().getSelectedItem();
        setCurrentPainting(0);
    }

    @FXML
    private void navButtonClickHandler(ActionEvent actionEvent) {
        Button sender = (Button) actionEvent.getSource();
        if(sender == prevPainting) {
            if(currentPaintingIndex > 0) {
                setCurrentPainting(--currentPaintingIndex);
            }
        } else if(sender == nextPainting) {
            if(currentPaintingIndex + 1 < currentAuthor.getPaintingsCount()) {
                setCurrentPainting(++currentPaintingIndex);
            }
        }
    }

    private String chooseDataLocation() {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Wybierz folder z obrazami");
        File chosenDirectory = directoryChooser.showDialog(null);

        return (chosenDirectory == null ? null : chosenDirectory.toString());
    }

    private void verifyDataLocation() {
        if(paintingsPath == null) {
            System.exit(0);
        }
    }

    private void loadData() {
        File folder = new File(paintingsPath);
        File[] authorsMetadata = folder.listFiles(new AuthorFileFilter());
        if(authorsMetadata == null) {
            return;
        }
        for(File authorFile : authorsMetadata) {
            Scanner authorData;
            try {
                authorData = new Scanner(authorFile, CHARSET);
            } catch (FileNotFoundException e) {
                continue;
            }

            Author author = new Author(new SimpleStringProperty(authorData.nextLine().replaceAll("&nbsp;", "")));
            authorData.nextLine();

            while(authorData.hasNextLine()) {
                String[] paintingData = authorData.nextLine().replaceAll("\"", "").split("\t");
                Painting painting = new Painting(
                        new SimpleStringProperty(paintingData[1]),
                        new SimpleStringProperty(paintingData[2]),
                        new SimpleStringProperty(""),
                        new SimpleStringProperty(paintingData[0])
                );
                author.insertPainting(painting);
            }
            authors.add(author);
        }
        selectAuthor.setItems(FXCollections.observableList(authors));
        selectAuthor.getSelectionModel().selectFirst();
    }

    private void setCurrentPainting(int paintingIndex) {
        currentPaintingIndex = paintingIndex;
        Painting currentPainting = currentAuthor.getPaintings().get(currentPaintingIndex);

        paintingTitle.setText(currentPainting.getTitle());
        paintingDescription.setText(currentPainting.getDescription());
        paintingLocation.setText(currentPainting.getLocation());

        File file = new File(paintingsPath + "\\" + currentPainting.getFilename());
        Image image = new Image(file.toURI().toString());
        painting.setImage(image);
    }
}
