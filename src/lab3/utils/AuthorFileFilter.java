package lab3.utils;

import java.io.File;
import java.io.FilenameFilter;

public class AuthorFileFilter implements FilenameFilter {
    @Override
    public boolean accept(File dir, String name) {
        return name.endsWith(".txt");
    }
}
