package lab3.model;

import javafx.beans.property.StringProperty;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class Author {
    private StringProperty name;
    private List<Painting> paintings;

    public Author(StringProperty name) {
        this.name = name;
        paintings = new LinkedList<>();
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void insertPainting(Painting painting) {
        paintings.add(painting);
    }

    public void insertPaintings(Collection<Painting> paintings) {
        this.paintings.addAll(paintings);
    }

    public List<Painting> getPaintings() {
        return new LinkedList<>(paintings);
    }

    public int getPaintingsCount() {
        return paintings.size();
    }

    @Override
    public String toString() {
        return getName();
    }
}
