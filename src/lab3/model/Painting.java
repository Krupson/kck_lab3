package lab3.model;

import javafx.beans.property.StringProperty;

public class Painting {
    private StringProperty title;
    private StringProperty description;
    private StringProperty location;
    private StringProperty filename;



    public Painting(StringProperty title, StringProperty description, StringProperty location, StringProperty filename) {
        this.title = title;
        this.description = description;
        this.location = location;
        this.filename = filename;
    }

    public String getTitle() {
        return title.get();
    }

    public StringProperty titleProperty() {
        return title;
    }

    public String getDescription() {
        return description.get();
    }

    public StringProperty descriptionProperty() {
        return description;
    }

    public String getLocation() {
        return location.get();
    }

    public StringProperty locationProperty() {
        return location;
    }

    public String getFilename() {
        return filename.get();
    }

    public StringProperty filenameProperty() {
        return filename;
    }
}
